function selectCoverul(className) {
	this.className = className;
	this.init();
}

selectCoverul.prototype = {

	init : function(){
		document.addEventListener('click', function(e) {
		    e = e || window.event;
		    if(e.target.tagName == 'LI') {
		    	var target = e.target || e.srcElement,
		        text = target.textContent || text.innerText;
		        target.parentNode.parentNode.getElementsByTagName('span')[0].innerHTML=text;
		    }
		}, false);
		this.replaceTags(this.className);
	},

	replaceTags : function(){
		var className = this.className.split(',');
		for(var i=0;i<className.length;i++){
			var getSelect = document.getElementsByClassName(className[i]);
			for(var j = 0; j<getSelect.length; j++) {
				var elementString = this.convertDOMtoString(getSelect[j]),
					optionName = getSelect[j].getElementsByTagName('option')[0].textContent;
				elementString = this.replaceAll('option','li',elementString);
				elementString = this.replaceAll('select','ul',elementString);
				getSelect[j].parentNode.innerHTML ='<span class="click">'+optionName+'</span>' + elementString;
			}
		}
	},

	convertDOMtoString : function(DOM) {
		if (DOM.outerHTML)
		    return DOM.outerHTML;
		else if (XMLSerializer)
		    return new XMLSerializer().serializeToString(DOM); 
	},

	replaceAll : function(find, replace, str) {
	  return str.replace(new RegExp(find, 'g'), replace);
	}
}


