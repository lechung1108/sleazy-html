;(function ($) {
    

	"use strict";

 
	jQuery(document).ready(function($) {




		/* Responsive menu*/
			if($("#mp-menu").length > 0)
			new mlPushMenu(document.getElementById( 'mp-menu' ), document.getElementById( 'sys_btn_toogle_menu' ),{dockSide: "right"} ); 




		/*Responsive Menu on the IE9*/
	    var rex = new RegExp("MSIE 9.0");
	    var trueIE = rex.test(navigator.userAgent);

	    var flag_show = false;  
	        if(trueIE) {
	            
	            $(".mp-menu").hide();
	            $(".has-sub .mp-level").hide();
	            //  Show menu
	            $(".btn-toogle-res-menu").click(function(ev) {
	                ev.stopPropagation();
	                $(".mp-menu").toggle("drop",{direction: "right"},300);          
	            });
	             // Show sub-menu
	            $(".has-sub").click(function(event){
	                    event.stopPropagation();
	                    $(this).children(".mp-level").show("slide",{direction: "right"},300);
	                })
	            // Close sub-menu
	            $('.mp-back').click(function(){
	                $(this).parent().hide("slide",{direction: "right"},300);
	            })
	            
	    	}
	     /*END IE 9*/
	     

	     

		/**
	     * jQuery.browser.mobile (http://detectmobilebrowser.com/)
	     *
	     * jQuery.browser.mobile will be true if the browser is a mobile device
	     *
	     **/
	    (function(a){(jQuery.browser=jQuery.browser||{}).mobile=/(android|bb\d+|meego).+mobile|avantgo|bada\/|blackberry|blazer|compal|elaine|fennec|hiptop|iemobile|ip(hone|od)|iris|kindle|lge |maemo|midp|mmp|mobile.+firefox|netfront|opera m(ob|in)i|palm( os)?|phone|p(ixi|re)\/|plucker|pocket|psp|series(4|6)0|symbian|treo|up\.(browser|link)|vodafone|wap|windows (ce|phone)|xda|xiino/i.test(a)||/1207|6310|6590|3gso|4thp|50[1-6]i|770s|802s|a wa|abac|ac(er|oo|s\-)|ai(ko|rn)|al(av|ca|co)|amoi|an(ex|ny|yw)|aptu|ar(ch|go)|as(te|us)|attw|au(di|\-m|r |s )|avan|be(ck|ll|nq)|bi(lb|rd)|bl(ac|az)|br(e|v)w|bumb|bw\-(n|u)|c55\/|capi|ccwa|cdm\-|cell|chtm|cldc|cmd\-|co(mp|nd)|craw|da(it|ll|ng)|dbte|dc\-s|devi|dica|dmob|do(c|p)o|ds(12|\-d)|el(49|ai)|em(l2|ul)|er(ic|k0)|esl8|ez([4-7]0|os|wa|ze)|fetc|fly(\-|_)|g1 u|g560|gene|gf\-5|g\-mo|go(\.w|od)|gr(ad|un)|haie|hcit|hd\-(m|p|t)|hei\-|hi(pt|ta)|hp( i|ip)|hs\-c|ht(c(\-| |_|a|g|p|s|t)|tp)|hu(aw|tc)|i\-(20|go|ma)|i230|iac( |\-|\/)|ibro|idea|ig01|ikom|im1k|inno|ipaq|iris|ja(t|v)a|jbro|jemu|jigs|kddi|keji|kgt( |\/)|klon|kpt |kwc\-|kyo(c|k)|le(no|xi)|lg( g|\/(k|l|u)|50|54|\-[a-w])|libw|lynx|m1\-w|m3ga|m50\/|ma(te|ui|xo)|mc(01|21|ca)|m\-cr|me(rc|ri)|mi(o8|oa|ts)|mmef|mo(01|02|bi|de|do|t(\-| |o|v)|zz)|mt(50|p1|v )|mwbp|mywa|n10[0-2]|n20[2-3]|n30(0|2)|n50(0|2|5)|n7(0(0|1)|10)|ne((c|m)\-|on|tf|wf|wg|wt)|nok(6|i)|nzph|o2im|op(ti|wv)|oran|owg1|p800|pan(a|d|t)|pdxg|pg(13|\-([1-8]|c))|phil|pire|pl(ay|uc)|pn\-2|po(ck|rt|se)|prox|psio|pt\-g|qa\-a|qc(07|12|21|32|60|\-[2-7]|i\-)|qtek|r380|r600|raks|rim9|ro(ve|zo)|s55\/|sa(ge|ma|mm|ms|ny|va)|sc(01|h\-|oo|p\-)|sdk\/|se(c(\-|0|1)|47|mc|nd|ri)|sgh\-|shar|sie(\-|m)|sk\-0|sl(45|id)|sm(al|ar|b3|it|t5)|so(ft|ny)|sp(01|h\-|v\-|v )|sy(01|mb)|t2(18|50)|t6(00|10|18)|ta(gt|lk)|tcl\-|tdg\-|tel(i|m)|tim\-|t\-mo|to(pl|sh)|ts(70|m\-|m3|m5)|tx\-9|up(\.b|g1|si)|utst|v400|v750|veri|vi(rg|te)|vk(40|5[0-3]|\-v)|vm40|voda|vulc|vx(52|53|60|61|70|80|81|83|85|98)|w3c(\-| )|webc|whit|wi(g |nc|nw)|wmlb|wonu|x700|yas\-|your|zeto|zte\-/i.test(a.substr(0,4))})(navigator.userAgent||navigator.vendor||window.opera);
	    
	    var mobile = jQuery.browser.mobile;

	    if( !mobile ) {

	        new WOW().init();
	        
	    }

		$('.tooltip-hover').tooltip();

		$('#sl-tab a').click(function (e) {
			  e.preventDefault();
			  $(this).tab('show')
			});
		
		if( $("#owl-one").length > 0 ) {
			$("#owl-one").owlCarousel({
 
				navigation : true, // Show next and prev buttons
				slideSpeed : 300,
				paginationSpeed : 400,
				singleItem:true,
				navigationText : ['<i class="fa fa-angle-left"></i>','<i class="fa fa-angle-right"></i>'],
			pagination : false
			 
			});
		}
		 
		if( $("#owl-one-blog").length > 0 ) {
		 $("#owl-one-blog").owlCarousel({
 
			navigation : true, // Show next and prev buttons
			slideSpeed : 300,
			paginationSpeed : 400,
			singleItem:true,
			navigationText : ['<i class="fa fa-angle-left"></i>','<i class="fa fa-angle-right"></i>'],
			pagination : false
			 
			// "singleItem:true" is a shortcut for:
			// items : 1,
			// itemsDesktop : false,
			// itemsDesktopSmall : false,
			// itemsTablet: false,
			// itemsMobile : false
			 
			});
		}

		if( $("#owl-four").length > 0 ) {
		 $("#owl-four").owlCarousel({

		 	slideSpeed : 300,
			paginationSpeed : 400,
			navigationText : ['<i class="fa fa-angle-left"></i>','<i class="fa fa-angle-right"></i>'],
			 itemsCustom : [
				[0, 1],
				[450, 1],
				[600, 1],
				[700, 2],
				[1000, 4],
				[1200, 4],
				[1400, 4],
				[1600, 4]
				],
				pagination : false,
				navigation : true
			 
			});
		}

		if( $("#owl-preview").length > 0 ) {
		 $("#owl-preview").owlCarousel({

		 	slideSpeed : 300,
			paginationSpeed : 400,
			navigationText : ['<i class="fa fa-angle-left"></i>','<i class="fa fa-angle-right"></i>'],
			 itemsCustom : [
				[0, 1],
				[450, 1],
				[600, 1],
				[700, 2],
				[1000, 3],
				[1200, 3],
				[1400, 3],
				[1600, 3]
				],
				pagination : false,
				navigation : true
			 
			});
		}


		$("#content-testimonials").owlCarousel({
            autoPlay: true,
            slideSpeed : 1000,
            navigation: false,
            pagination: false,
            items : 1,
            singleItem: true,
            autoHeight: true
        });


		// Isotope

	 	var container = $('#products-wrap');
	    container.isotope({
	        animationEngine : 'best-available',
	        animationOptions: {
	            duration: 300,
	            queue: false
	        },
	        layoutMode: 'fitRows',

	        //  Sort Ascending A-Z 
	        getSortData: {
	        	name_asc: function(elem){
	        		var name_product = $(elem).find(".item-title > a").text();
	        		return name_product;
	        	},
	        	name_desc: function(elem){
	        		var name_product = $(elem).find(".item-title > a").text();
	        		return name_product;	
	        	}
	        }
	    });

	    $('#filters .isotope-tab li:eq(0) a').addClass('select-filter');

	    $('#filters .isotope-tab a').click(function(event){
	    	event.preventDefault();
	        $('#filters .isotope-tab a').removeClass('select-filter');
	        $(this).addClass('select-filter');

	        // Filting
	        var selector = $(this).attr('data-filter');
	        container.isotope({ filter: selector});
	        return false;
	    });

	    // Sorting 
	    $('#sortByNameAsc').click(function(event) {
	    	event.preventDefault();
		    container.isotope({
		    	sortBy : 'name_asc', 
		    	sortAscending : true
		    });
		});

		$('#sortByNameDesc').click(function(event) {
			event.preventDefault();
		    container.isotope({
		    	sortBy : 'name_desc', 
		    	sortAscending : false
		    });
		});


		//Sidebar nav
		//increase/ decrease product qunatity buttons +/- in cart.html table
        if(jQuery('.dropdown-icon')[0]){
            jQuery('.dropdown-icon').click(function(e){
            	e.preventDefault();
                jQuery(this).toggleClass('icon-right');
                jQuery(this).toggleClass('icon-down');
                jQuery(this).toggleClass('active');
                jQuery(this).parent('li').find('ul').first().slideToggle();
            });
        }

        // Responsive Tabs
        $(window).resize(function(){
        	if(window.innerWidth < 768) {
	        	$(".amz-tabs .collapse").removeClass("tab-pane active");
        	}
        })
        $(window).load(function(){}).trigger('resize');


       


        // Event Click Cart in the header on the mobile devices
        $(".top-cart-wrapper").click(function(){
        	$(this).toggleClass("active");
        })


        

	    // Cover default select to dropdown ul li 
		// var newObject = new selectCoverul('select-dropdown');	    




	});



})(jQuery);