jQuery(document).ready(function($) {
	
	//  Price Filter ( noUiSlider Plugin)
    $("#price-range").noUiSlider({
		range: {
			'min': [ 0 ],
			'max': [ 100 ]
		},
		start: [20, 80],
        connect:true,
        serialization:{
            lower: [
				$.Link({
					target: $("#price-min")
				})
			],
			upper: [
				$.Link({
					target: $("#price-max")
				})
			],
			format: {
			// Set formatting
				decimals: 2,
				prefix: '$'
			}
        }
	})
})